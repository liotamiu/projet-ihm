package fr.liotapsi.vuegraphique.models;

import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;

public class DessinModele extends Observable {

    private int nbClic;

    private ArrayList<Point> point_cliques;

    private FigureColoree figureEnCours;

    private ArrayList<FigureColoree> lfg;

    public DessinModele() {

    }

    public void ajoute(FigureColoree figureColoree) {

    }

    public void changeCoul(FigureColoree figureColoree, Color color) {

    }

    public void construit(FigureColoree figureColoree) {

    }

    public void ajoutePt(int x, int y) {

    }

    public int getNbClic() {
        return nbClic;
    }

    public void setNbClic(int nbClic) {
        this.nbClic = nbClic;
    }

    public ArrayList<FigureColoree> getLfg() {
        return this.lfg;
    }

    public FigureColoree getFigureEnCours() {
        return this.figureEnCours;
    }

    public void setFigureEnCours(FigureColoree figureEnCours) {
        this.figureEnCours = figureEnCours;
    }

    public void setLfg(ArrayList<FigureColoree> figureColoreeArrayList) {
        this.lfg = figureColoreeArrayList;
    }

}
