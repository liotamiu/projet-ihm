package fr.liotapsi.vuegraphique.models;

public class Point {

    private int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(Point point) {
        double p1_x = (double) this.x, p1_y = (double) this.y;
        double p2_x = (double) point.x, p2_y = (double) point.y;
        return Math.sqrt(Math.pow(p1_x - p2_x, 2) +  Math.pow(p1_y - p2_y, 2));
    }

    public int rendreX() {
        return this.x;
    }

    public int rendreY() {
        return this.y;
    }

    public void incrementerX(int x) {
        this.x += x;
    }

    public void incrementerY(int y) {
        this.y += y;
    }

    public void modifierX(int x) {
        this.x = x;
    }

    public void modifiery(int y) {
        this.y = y;
    }

    public void translation(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
