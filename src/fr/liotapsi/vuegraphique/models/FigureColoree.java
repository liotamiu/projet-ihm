package fr.liotapsi.vuegraphique.models;

import java.awt.*;

public class FigureColoree {

    private int TAILLE_CARRE_SELECTION;

    private boolean selected;

    private Color couleur;

    public FigureColoree() {

    }

    public int nbPoints() {
        return -1;
    }

    public int nbClics() {
        return -1;
    }

    public void modifierPoints(Point[] p) {
    }

    public void affiche(Graphics g) {
    }

    public void selectionne() {
    }

    public void deSelectionne() {
    }

    public void changerCouleur(Color c) {
    }


}
