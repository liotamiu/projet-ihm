package fr.liotapsi.vuegraphique.controler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe pour gérer les ComboBox et les RadioButton
 */
public class CurrentAction implements ActionListener {

    /**
     * variable qui stock les ComboBox
     */
    private JComboBox form, color;

    /**
     * variable qui indique à quels sont les actions en cours
     */
    private boolean isForm, isDraw, isManipulated;

    public CurrentAction(JComboBox form, JComboBox color) {
        this.form = form;
        this.color = color;
    }

    /**
     * récupération des actions en cours
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Nouvelle figure":
                this.form.setEnabled(true);
                this.color.setEnabled(true);
                this.isForm = true;
                this.isDraw = false;
                this.isManipulated = false;
                break;
            case "Tracé à main levée":
                this.form.setEnabled(false);
                this.color.setEnabled(true);
                this.isForm = false;
                this.isDraw = true;
                this.isManipulated = false;
                break;
            case "Manipulations":
                this.form.setEnabled(false);
                this.color.setEnabled(false);
                this.isForm = false;
                this.isDraw = false;
                this.isManipulated = true;
                break;
        }
    }

    /**
     * demander le status de l'action en cours
     *
     * @return vrai si il y a manipulation des objets
     */
    public boolean isManipulated() {
        return isManipulated;
    }

    /**
     * demander le status de l'action en cours
     *
     * @return vrai si dessin a main levée est en cours
     */
    public boolean isDraw() {
        return isDraw;
    }

    /**
     * demander le status de l'action en cours
     *
     * @return vrai si il y a utilisation de polygone en cours
     */
    public boolean isForm() {
        return isForm;
    }
}
