package fr.liotapsi.vuegraphique.controler;

import fr.liotapsi.vuegraphique.models.DessinModele;
import fr.liotapsi.vuegraphique.models.FigureColoree;

import javax.swing.*;
import java.awt.*;

public class PanneauxChoix extends JPanel {

    public PanneauxChoix(DessinModele dessinModele) {
        this.setLayout(new BorderLayout());
        // line 1
        JPanel line_1 = new JPanel();
        JRadioButton new_fig = new JRadioButton("Nouvelle figure");
        JRadioButton manual_draw = new JRadioButton("Tracé à main levée");
        JRadioButton manipulation = new JRadioButton("Manipulations");
        ButtonGroup group = new ButtonGroup();
        group.add(new_fig);
        line_1.add(new_fig);
        group.add(manual_draw);
        line_1.add(manual_draw);
        group.add(manipulation);
        line_1.add(manipulation);
        this.add(line_1, BorderLayout.NORTH);
        // line 2
        JPanel line_2 = new JPanel();
        JComboBox form = new JComboBox();
        form.addItem("triangle");
        form.addItem("carré");
        form.addItem("rond");

        JComboBox color = new JComboBox();
        color.addItem("bleu");
        color.addItem("rouge");
        color.addItem("vert");
        color.addItem("magenta");

        CurrentAction currentAction = new CurrentAction(form, color);
        new_fig.addActionListener(currentAction);
        manual_draw.addActionListener(currentAction);
        manipulation.addActionListener(currentAction);
        line_2.add(form);
        line_2.add(color);
        this.add(line_2, BorderLayout.SOUTH);
    }

    public FigureColoree creeFigure(int i) {
        return null;
    }


    public Color determineCouleur(int value) {
        return null;
    }

}
