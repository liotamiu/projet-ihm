package fr.liotapsi.vuegraphique.main;

import fr.liotapsi.vuegraphique.controler.PanneauxChoix;
import fr.liotapsi.vuegraphique.view.VueDessin;

import javax.swing.*;
import java.awt.*;

public class Fenetre extends JFrame {

    private JPanel principal;

    private JPanel choix;

    public Fenetre(String name, int x, int y) {
        this.setSize(x, y);
        this.setTitle(name);
        this.principal = new VueDessin();
        this.choix = new PanneauxChoix(null);
        this.add(this.choix, BorderLayout.NORTH);
        this.setDefaultCloseOperation(3);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new Fenetre("Figures Géométriques", 700, 700);
    }
}
